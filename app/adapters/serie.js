import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import config from 'dashboard/config/environment';

import { inject as service } from '@ember/service';
import { isPresent } from '@ember/utils';

const { RESTAdapter } = DS;

export default RESTAdapter.extend(DataAdapterMixin, {
  host: config.apiUrl,
  namespace: config.apiNamespace + '/users/5c0d496008257d00220897e0',
  session: service(),
  authorize(xhr) {
    let { access_token } = this.get('session.data.authenticated');
    if (isPresent(access_token)) {
      xhr.setRequestHeader('Authorization', `Bearer ${access_token}`);
    }
  },
  pathForType(type) {
    return 'books';
  }
});
