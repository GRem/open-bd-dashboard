import Route from '@ember/routing/route';
import RSVP from 'rsvp';

export default Route.extend({
  model: function({ user_id }) {
    return RSVP.hash({
      user: this.store.findRecord('user', user_id),
      // serie: this.store.findAll('serie', { user: user_id })
      serie: this.store.findAll('serie')
    });
  }
});
