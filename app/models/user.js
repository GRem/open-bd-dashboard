import DS from 'ember-data';

export default DS.Model.extend({
  email: DS.attr('string'),
  pseudo: DS.attr('string'),
  role: DS.attr('string'),
  items: DS.attr('number'),
  books: DS.belongsTo('serie', { async: true })
});
